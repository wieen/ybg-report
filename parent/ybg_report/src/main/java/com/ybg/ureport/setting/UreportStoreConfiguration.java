package com.ybg.ureport.setting;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.bstek.ureport.provider.report.ReportFile;
import com.bstek.ureport.provider.report.ReportProvider;
import com.ybg.base.jdbc.BaseMap;
import com.ybg.base.jdbc.util.QvoConditionUtil;
import com.ybg.report.rtemplate.domain.RtemplateVO;
import com.ybg.report.rtemplate.qvo.RtemplateQuery;
import com.ybg.report.rtemplate.service.RtemplateService;
import cn.hutool.core.date.DateUtil;

/** UReport 报表模板存储设置。如果没有 就会去调用默认的存储形式（文件） 2018年2月20日 21:03:05
 * 
 * @author 88ybg */
@Component
public class UreportStoreConfiguration implements ReportProvider {
	
	@Autowired
	RtemplateService	rtemplateService;
	/** 报表文件名前缀 **/
	private String		prefix	= "database:";
	///** 该属性的值用于定义UReport2中提供的默认基于文件系统的报表存储目录 **/
	//private String		fileStoreDir;
	/** 是否禁用 **/
	private boolean		disabled;
	
	/** 根据报表名加载报表文件
	 * 
	 * @param file
	 *            报表名称
	 * @return 返回的InputStream */
	@Override
	public void deleteReport(String file) {
		// if(file.startsWith(prefix)){
		// file=file.substring(prefix.length(),file.length());
		// }
		// String fullPath=fileStoreDir+"/"+file;
		// File f=new File(fullPath);
		// if(f.exists()){
		// f.delete();
		// }
		// 上面是默认实现
		BaseMap<String, Object> wheremap = new BaseMap<>();
		wheremap.put("filename", file);
		rtemplateService.remove(wheremap);
	}
	
	/** @return 返回是否禁用 */
	@Override
	public boolean disabled() {
		return disabled;
	}
	
	/** @return 返回存储器名称 */
	@Override
	public String getName() {
		return "数据库存储系统";
	}
	
	/** @return 返回报表文件名前缀 */
	@Override
	public String getPrefix() {
		return prefix;
	}
	
	/** 获取所有的报表文件
	 * 
	 * @return 返回报表文件列表 */
	@Override
	public List<ReportFile> getReportFiles() {
		// File file=new File(fileStoreDir);
		// List<ReportFile> list=new ArrayList<ReportFile>();
		// for(File f:file.listFiles()){
		// Calendar calendar=Calendar.getInstance();
		// calendar.setTimeInMillis(f.lastModified());
		// list.add(new ReportFile(f.getName(),calendar.getTime()));
		// }
		// Collections.sort(list, new Comparator<ReportFile>(){
		// @Override
		// public int compare(ReportFile f1, ReportFile f2) {
		// return f2.getUpdateDate().compareTo(f1.getUpdateDate());
		// }
		// });
		// return list;
		// 上面是默认实现。
		RtemplateQuery qvo = new RtemplateQuery();
		List<ReportFile> rlist = new ArrayList<>();
		List<RtemplateVO> list;
		try {
			list = rtemplateService.list(qvo);
			for (RtemplateVO bean : list) {
				ReportFile rbean = new ReportFile(bean.getFilename(), DateUtil.parseDate(bean.getGmtModified()));
				rlist.add(rbean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rlist;
	}
	
	/** 根据报表名加载报表文件
	 * 
	 * @param file
	 *            报表名称
	 * @return 返回的InputStream */
	@Override
	public InputStream loadReport(String file) {
		// if(file.startsWith(prefix)){
		// file=file.substring(prefix.length(),file.length());
		// }
		// String fullPath=fileStoreDir+"/"+file;
		// try {
		// return new FileInputStream(fullPath);
		// } catch (FileNotFoundException e) {
		// throw new ReportException(e);
		// }
		// 上面是默认实现。
		if (file.startsWith(prefix)) {
			file = file.substring(prefix.length(), file.length());
		}
		RtemplateQuery qvo = new RtemplateQuery();
		qvo.setFilename(file);
		List<RtemplateVO> list = new ArrayList<>();
		try {
			list = rtemplateService.list(qvo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (QvoConditionUtil.checkList(list)) {
			RtemplateVO bean = list.get(0);
			InputStream in_withcode;
			try {
				in_withcode = new ByteArrayInputStream(bean.getFilecontent().getBytes("UTF-8"));
				return in_withcode;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/** 保存报表文件
	 * 
	 * @param file
	 *            报表名称
	 * @param content
	 *            报表的XML内容 */
	@Override
	public void saveReport(String file, String content) {
		// if(file.startsWith(prefix)){
		// file=file.substring(prefix.length(),file.length());
		// }
		// String fullPath=fileStoreDir+"/"+file;
		// FileOutputStream outStream=null;
		// try{
		// outStream=new FileOutputStream(new File(fullPath));
		// IOUtils.write(content, outStream,"utf-8");
		// }catch(Exception ex){
		// throw new ReportException(ex);
		// }finally{
		// if(outStream!=null){
		// try {
		// outStream.close();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// }
		// }
		// 上面是默认实现
		if (file.startsWith(prefix)) {
			file = file.substring(prefix.length(), file.length());
		}
		RtemplateVO bean = new RtemplateVO();
		bean.setFilecontent(content);
		bean.setFilename(file);
		RtemplateQuery qvo = new RtemplateQuery();
		try {
			List<RtemplateVO> list = rtemplateService.list(qvo);
			if (QvoConditionUtil.checkList(list)) {
				try {
					BaseMap<String, Object> updatemap = new BaseMap<>();
					updatemap.put("filecontent", content);
					BaseMap<String, Object> wheremap = new BaseMap<>();
					wheremap.put("filename", file);
					rtemplateService.update(updatemap, wheremap);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else {
				try {
					rtemplateService.save(bean);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
