package com.ybg.report.rtable.service;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import com.ybg.report.rtable.dao.RtableDao;
import com.ybg.report.rtable.domain.RtableVO;
import com.ybg.report.rtable.qvo.RtableQuery;
import java.util.List;
import com.ybg.base.jdbc.BaseMap;
import com.ybg.base.util.Page;

/** @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-22 */
@Repository
public class RtableServiceImpl implements RtableService {
	
	@Autowired
	private RtableDao rtableDao;
	
	@Override
	public RtableVO save(RtableVO bean) throws Exception {
		return rtableDao.save(bean);
	}
	
	@Override
	public void update(BaseMap<String, Object> updatemap, BaseMap<String, Object> wheremap) {
		rtableDao.update(updatemap, wheremap);
	}
	
	@Override
	public Page list(Page page, RtableQuery qvo) throws Exception {
		return rtableDao.list(page, qvo);
	}
	
	@Override
	public List<RtableVO> list(RtableQuery qvo) throws Exception {
		return rtableDao.list(qvo);
	}
	
	@Override
	public void remove(BaseMap<String, Object> wheremap) {
		rtableDao.remove(wheremap);
	}
	
	@Override
	public RtableVO get(String id) {
		return rtableDao.get(id);
	}
	
	@Override
	public void createDataTable(String datatablename) {
		rtableDao.createDataTable(datatablename);
	}
}
