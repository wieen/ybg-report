package com.ybg.report.datatable.dao;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.ybg.base.jdbc.BaseDao;
import com.ybg.base.jdbc.util.DateUtil;
import com.ybg.report.datatable.domain.DataTableDO;

@Repository
public class DataTableDaoImpl extends BaseDao implements DataTableDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public void saveDateRemoveOld(List<DataTableDO> list, String departmentno, String dataTableName) {
		removeDepartmentData(departmentno, dataTableName);
		StringBuilder sql = new StringBuilder();
		sql.append(" insert into " + dataTableName + " (departmentno,rrow,rcolumn,rvalue,gmt_create,gmt_modified)");
		sql.append(" values(?,?,?,?,?,?)");
		getJdbcTemplate().batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps, int index) throws SQLException {
				DataTableDO bean = list.get(index);
				int count = 1;
				ps.setString(count++, bean.getDepartmentno());
				ps.setInt(count++, bean.getRrow());
				ps.setInt(count++, bean.getRcolumn());
				ps.setString(count++, DateUtil.getDateTime());
				ps.setString(count++, DateUtil.getDateTime());
			}
			
			@Override
			public int getBatchSize() {
				return list.size();
			}
		});
	}
	
	@Override
	public void removeDepartmentData(String departmentno, String dataTableName) {
		StringBuilder sql = new StringBuilder();
		sql.append(" delete from ").append(dataTableName).append(" where departmentno='" + departmentno + "'");
		getJdbcTemplate().update(sql.toString());
	}
	
	@Override
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
}
