package com.ybg.report.departmentable.domain;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/** @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-21 */
@ApiModel("实体（数据库)")
public class DepartmenTableDO implements Serializable {
	
	private static final long	serialVersionUID	= 1L;
	/****/
	@ApiModelProperty(name = "id", dataType = "java.lang.String", value = "", hidden = false)
	private String				id;
	/****/
	@ApiModelProperty(name = "reportid", dataType = "java.lang.String", value = "", hidden = false)
	private String				reportid;
	/****/
	@ApiModelProperty(name = "departmentid", dataType = "java.lang.String", value = "", hidden = false)
	private String				departmentid;
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	public void setReportid(String reportid) {
		this.reportid = reportid;
	}
	
	public String getReportid() {
		return reportid;
	}
	
	public void setDepartmentid(String departmentid) {
		this.departmentid = departmentid;
	}
	
	public String getDepartmentid() {
		return departmentid;
	}
}
