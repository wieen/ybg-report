package com.ybg.report.datatable.dao;
import java.util.List;
import com.ybg.report.datatable.domain.DataTableDO;

public interface DataTableDao {
	
//	/** 简单插入 报错不管 如果漏了要覆盖的行列 并不处理（不推荐）
//	 * 
//	 * @param bean
//	 * @throws Exception
//	 */
//	@Deprecated
//	void onlysave(List<DataTableDO> bean) throws Exception;
//	
//	/** 替换插入 重复不覆盖 如果漏了要覆盖的行列 并不处理（不推荐）
//	 * 
//	 * @param bean
//	 */
//	@Deprecated
//	void saveWithOutReplace(List<DataTableDO> bean);
//	
//	/** 替换插入 重复覆盖 如果漏了要覆盖的行列 并不处理 （不推荐）
//	 * 
//	 * @param bean
//	 */
//	@Deprecated
//	void replaceSave(List<DataTableDO> bean);
	
	/** 先删除某个部门的 然后再插入数据（推荐）
	 * 
	 * @param list
	 *            部门数据
	 * @param departmentno
	 *            部门编码 */
	void saveDateRemoveOld(List<DataTableDO> list, String departmentno,String dataTableName);
	/**删除某个部门的数据**/
	void removeDepartmentData(String departmentno,String dataTableName);
}
