package com.ybg.report.util;
/** 行列工具类
 * 
 * @author 严宇
 * @date 2018年1月18日
 * @version 1.0.0 */
public class ExcelRowColUtil {
	
	private static int getCellNum(String cellStr) {
		char[] cellStrArray = cellStr.toUpperCase().toCharArray();
		int len = cellStrArray.length;
		int n = 0;
		for (int i = 0; i < len; i++) {
			n += (((int) cellStrArray[i]) - 65 + 1) * Math.pow(26, len - i - 1);
		}
		return n;
	}
	
	/** 解析出第几行 第几列 例如 A1 <br>
	 * 第一行 第一列 解析失败则返回0行0列
	 * 
	 * @param rowcol
	 *            如:A1
	 * @return */
	public static RowColumn analysisCellName(String rowcol) {
		if (rowcol == null) {
			return new RowColumn();
		}
		try {
			RowColumn bean = new RowColumn();
			String column = new RowColAnalysis(rowcol).printStr();
			bean.setColumn(getCellNum(column) - 1);
			String rowstr = new RowColAnalysis(rowcol).printNum();
			bean.setRow(Integer.parseInt(rowstr) - 1);
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			return new RowColumn();
		}
	}
//	public static void main(String[] args) {
//		System.out.println(analysisCellName("A2"));
//	}
}
