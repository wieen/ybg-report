package com.ybg.report.department.service;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import com.ybg.report.department.dao.DeptmentDao;

import com.ybg.report.department.domain.DeptmentVO;
import com.ybg.report.department.qvo.DeptmentQuery;
import java.util.List;

import com.ybg.base.jdbc.BaseMap;
import com.ybg.base.util.Page;

/**
 * 
 * 
 * @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-21
 */

@Repository
public class DeptmentServiceImpl implements DeptmentService {
	@Autowired
	private DeptmentDao deptmentDao;
	
	@Override	
	public DeptmentVO save(DeptmentVO bean) throws Exception{
	return deptmentDao.save(bean);
	
	}
	
	@Override
	public	void update(BaseMap<String, Object> updatemap, BaseMap<String, Object> wheremap){
		deptmentDao.update(updatemap,wheremap);
	}
	
	
	@Override
	public	Page list(Page page,DeptmentQuery qvo)throws Exception{
		return deptmentDao.list(page,qvo);
	}
	
	@Override
	public	List<DeptmentVO> list(DeptmentQuery qvo) throws Exception{
		return deptmentDao.list(qvo);
	}
   @Override
	public void remove(BaseMap<String, Object> wheremap){
		deptmentDao.remove(wheremap);
	}
   @Override
	public DeptmentVO get(String id){
		return 	deptmentDao.get(id);
	}
}
