package com.ybg.report.rtable.qvo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.ybg.base.jdbc.BaseQueryAble;
import com.ybg.report.rtable.domain.RtableDO;


/**
 * 
 * 
 * @author Deament
 * @email 591518884@qq.com
 * @date 2018-02-22
 */
@ApiModel(value = "角色查询条件", parent = RtableDO.class)
public class RtableQuery extends RtableDO implements BaseQueryAble {
	
	private static final long	serialVersionUID	= -1516990738029741157L;
	/** 是否模糊查询 **/
	@ApiModelProperty(name = "blurred", dataType = "java.lang.Boolean", value = "是否模糊查询", hidden = true)
	private boolean				blurred;
	
	public boolean isBlurred() {
		return blurred;
	}
	
	public void setBlurred(boolean blurred) {
		this.blurred = blurred;
	}
	
	
}