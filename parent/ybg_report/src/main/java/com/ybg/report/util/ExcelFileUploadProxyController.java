package com.ybg.report.util;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;
import com.ybg.upload.LocalUploadConstant;
import cn.hutool.core.convert.Convert;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

/** 代理类 只是类似于Service 的工作 返回 需要的文件对象
 * 
 * @author 严宇
 * @date 2018年1月26日
 * @version 1.0.0 */
public class ExcelFileUploadProxyController {
	
	/** 文件上传后 如果是excel 处理的controller 则返回Workbook给控制层继续处理
	 * 
	 * @throws FileNotFoundException
	 **/
	public static Workbook getWorkbookByController(MultipartFile file) throws FileNotFoundException {
		String uploadDir = LocalUploadConstant.BASEPATH;
		String filename = null;
		if (!file.isEmpty()) {
			try {
				// 如果目录不存在，自动创建文件夹
				LocalUploadConstant.createdir(uploadDir);
				// 文件后缀名
				String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
				// 上传文件名
				filename = UUID.randomUUID() + suffix;
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(LocalUploadConstant.BASEPATH + new File(filename)));
				out.write(file.getBytes());
				out.flush();
				out.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		File excelFile = new File(uploadDir, filename);
		return ExcelUtil.loadBook(excelFile);
	}
	
	/** 得到excel 区域数据
	 * 
	 * @param book
	 *            Excel 类
	 * @param index
	 *            sheet第几个 0代表第一个
	 * @param startrow
	 *            开始的行 0开始
	 * @param startcol
	 *            开始的列 0开始
	 * @param endrow
	 *            结束的行 0开始
	 * @param endcol
	 *            结束的列 0开始
	 * @return */
	public static String[][] getrender(Workbook book, Integer sheetindex, int startrow, int startcol, int endrow, int endcol) {
		if (sheetindex == null) {
			sheetindex = 0;
		}
		String[][] cellvalues = new String[endrow - startrow + 1][endcol - startcol + 1];
		ExcelReader bean = new ExcelReader(book, sheetindex);
		int i = 0;
		for (int y = startrow; y <= endrow; y++) {
			int j = 0;
			for (int x = startcol; x <= endcol; x++) {
				cellvalues[i][j] = Convert.toStr(bean.readCellValue(x, y));
				j++;
			}
			i++;
		}
		return cellvalues;
	}
}
